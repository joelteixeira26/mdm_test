#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    Created on Mon Jan 20 10:09:05 2020

    @author: Joel Teixeira
"""
import sys
import os

from query_strings import QueryStrings

if __name__ == "__main__":
    my_queries = sys.argv[1].split(',')

    # if only one argument is given : I get the environnement variable
    if len(sys.argv) <= 2:
        my_strings = os.getenv('MDM_STRINGS', [])
    else:
        my_strings = sys.argv[2].split(',')

    if len(my_strings) > 0:
        query_in_strings = QueryStrings(strings=my_strings, queries=my_queries)
        occurences = query_in_strings.count_queries_in_strings()

        print(dict(zip(my_queries, occurences)), flush=True)

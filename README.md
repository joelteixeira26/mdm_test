# MDM_test
Test technique MdM. 20/01/2020

For a given set of input strings and a given set of query strings, the application determines how many times each query string occurs in the list of input strings.

## Running the application

The application can be executed with the following command:

On windows

```$ python -m main queries strings```

on UNIX systems:

```$ python3 -m main queries strings```

Each argument (`queries` and `strings`) is a sequence of characters with a comma `,` delimiting the elements of the set.

If `strings` is not given, the application will fetch the value of the environment variable 'MDM_STRINGS'.

If this environnement variable is not available, then no answer will be given.

The command returns a dictionary in the form : `{query : n_occurences}` where `n_occurences` is the number of ocurrences of `query` in `<queries>`.

For example, if one want to determine the number of occurences of each element of the set `['ab','abc','bc']` (queries) in the set `['ab','ab','abc']` (strings), one must execute:

```$ python3 -m main ab,abc,bc ab,ab,abc ```

and the command returns:

```{'ab': 2, 'abc': 1, 'bc': 0}```


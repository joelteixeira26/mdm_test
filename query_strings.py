#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    Created on Mon Jan 20 09:45:23 2020

    @author: Joel Teixeira
"""


class QueryStrings:

    def __init__(self, strings, queries):
        """
        :type strings: list of strings
        :type queries: list of strings
        """
        self.set_strings(strings)
        self.set_queries(queries)

    def set_strings(self, strings):
        self.strings = strings

    def set_queries(self, queries):
        self.queries = queries

    def count_queries_in_strings(self):
        return [self.strings.count(q) for q in self.queries]
